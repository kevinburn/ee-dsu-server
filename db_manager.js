var ex = (function () {
    var _inited = false;
    var _connection = null;
    const _connectionInfo ={
      host: '',
      port: 3306,
      user: '',
      password: '',
      database: ""
    };
    const _mysql = require("mysql");
    return {
      init: function (onConnected, onError) {
        if(!_inited) {
          _connection = _mysql.createConnection(_connectionInfo);
          _connection.connect(function(err) {
            if(err == null) {
              _inited = true;
              if(onConnected) {
                onConnected();
              }
            }
            else {
              if(onError) {
                onError(err);
              } else {
                throw err;
              }
            }
          });
        }
      },
      destroy() {
        if(_inited) {
          _connection.end();
        }
      },
      insertMany: function(table, cols, values, onSuccess, onError) {
        if(_inited) {
          _connection.query('INSERT INTO ' + table + ' (' + cols + ') VALUES ?', [values], function(err, result) {
            if(err == null){
              if(onSuccess){
                onSuccess(result);
              }
            }
            else {
              if(onError) {
                onError(err);
              }
            }
          });
        }
      },
      execQuery: function(query, onSuccess, onError) {
        if(_inited) {
          _connection.query(query, function(err, result) {
            if(err == null){
              if(onSuccess){
                onSuccess(result);
              }
            }
            else {
              if(onError) {
                onError(err);
              }
            }
          });
        }
      }
    };
}());
Object.freeze(ex);
module.exports = ex;
