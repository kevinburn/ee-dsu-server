/**
Sites:
id INT, Primary Key, AUTO_INCREMENT
power DECIMAL (13, 4)
dsu_id INT, Foreign Key to dsus.id

dsus:
id INT, Primary Key, AUTO_INCREMENT
power DECIMAL(13,4)

power_data:
dsu_id INT, Primary Key, Foreign key to dsus.id
total_power DECIMAL(13,4)
time_aggregated UTC DATETIME, Primary Key

ASSUMPTIONS:
time_aggregated is the UTC_TIMESTAMP at which the power was aggregated and inserted into the table
*/

var dbMan = require("./db_manager");
const http = require('http');
const hostname = '127.0.0.1';
const port = 3000;
const POWER_DATA_UPDATE_RATE = 1000;//Frequency in ms that we should update power_data table

const server = http.createServer();

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
  dbMan.init(powerUpdateLoop);
});

process.on('SIGINT', function() {
  dbMan.destroy();
  process.exit();
});

/**
* Search all sites and aggregate the total power usage by dsu
* NOTE: I have included a constant to include the DSU power when calculating the total power for each DSU's sites
* This has been defaulted to off as including dsus.power does not apprear to be mentioned in the spec.
* @param cols An array of the cols that we are expecting in the order we want them returned in
* @param onSuccess The function to call onSuccess, will be passed either the array of results or null if there was an error
*/
var getPowerByDSU = function(onSuccess) {
  const INCLUDE_DSU_POWER = false;
  var query = "SELECT S.dsu_id, SUM(S.power) as total_site_power, D.power as dsu_power FROM sites S inner join dsus D on S.dsu_id=D.id GROUP BY D.id";
  dbMan.execQuery(query, function(results) {
    if(results.length > 0) {
      var powerByDSU = [];
      var timeAggregated = new Date().toISOString().substring(0, 19).replace('T', ' ');

      for(var i = 0; i < results.length; i++) {
        if(INCLUDE_DSU_POWER) {
          powerByDSU.push([ results[i].dsu_id, results[i].total_site_power + results[i].dsu_power, timeAggregated]);
        } else {
          powerByDSU.push([ results[i].dsu_id, results[i].total_site_power, timeAggregated]);
        }
      }
      if(typeof onSuccess == "function") {
        onSuccess(powerByDSU);
      }
    }
    else if(typeof onSuccess == "function") {
      onSuccess([]);
    }
  });
};

/**
* Updating the power_data table
*/
var onPowerUpdate = function() {
  var cols = ["dsu_id", "total_power", "time_aggregated"];
  getPowerByDSU(function(values) {
    dbMan.insertMany('power_data', cols, values, function(result) {
      console.log("inserted");
      console.log(values);
    }, function(err) {
      switch(err.code) {
        case "ER_DUP_ENTRY":
          console.error("already added!");
      }
      console.error(err.code);
    });
  });
};

/**
* This function will run the onPowerUpdate function at regular intervals
* Interval in milliseconds is determined by POWER_DATA_UPDATE_RATE
* NOTE: From running the commented out overflow count code I can estimate that
* this loop misses a second approximately every 150 seconds
* NOTE: If we update the power_data table more than once per second then we will get unintended behaviour
* in the form of primary key errors due to the spec not establishing power_data's DATETIME as having decimal places.
* If we want more frequent updates we would have to alter the table. I've assumed for the purpose of this challenge that that is impossible
* For this reason I have included a const MINIMUM_POWER_UPDATE_RATE to stop the POWER_DATA_UPDATE_RATE from being too low
*/
var powerUpdateLoop = function() {
  var totalRuns = 0;
  const MINIMUM_POWER_UPDATE_RATE = 1000;//Don't change this unless you
  if(POWER_DATA_UPDATE_RATE < MINIMUM_POWER_UPDATE_RATE) {
    console.error("Cannot have power update rate lower than " + MINIMUM_POWER_UPDATE_RATE + "ms. See powerUpdateLoop notes for details.")
  }
  else {
    /*var currentOverflow = 0;
    var overflowRuns = 0;*/
    var lastRun = new Date().getTime();
    var intervalId = setInterval(function() {
        var timeElapsed = new Date().getTime() - lastRun;
        if(timeElapsed > POWER_DATA_UPDATE_RATE) {
          /*totalRuns++;
          currentOverflow += (timeElapsed - POWER_DATA_UPDATE_RATE);
          if((currentOverflow / 1000) > (overflowRuns + 1)){
            overflowRuns++;
            console.log("Missed " + overflowRuns + " out of " + totalRuns);
          }*/
          onPowerUpdate();
          lastRun = new Date().getTime();
        }
      }, 10);
  }
};
