# Back end development challenge
This is Kevin Beirne's submission for the back end development challenge issued by Paul McCarthy. A copy of the challenge spec is included in this repository.


## Technology
This app uses node and mysql to create a server that does database updates according to the spec. The application can be started using npm.

## Installation
- Install the latest version of node (8.11.3 at time of writing.)
- Clone this repository
- Open db_manager.js and fill in the relevant, host, port, user, password and database
- Run:
``npm install``
``npm start``

## Assumptions
- time_aggregated is the UTC_TIMESTAMP at which the power was aggregated and inserted into the table
- time_aggregated is a UTC_TIMESTAMP with no decimal places and therefore more than one insertion into the power_data table per dsu_id per second should be disallowed

## Other notes
- I could not see a use for the power value in the dsus table in the spec. However, I thought it might be possible that this was to be included in the aggregation function so I included a constant INCLUDE_DSU_POWER (default false) to enable this if you so choose. See getPowerByDSU in main.js for more details.
- The error handling in this application is simplistic. This, along with other quality of life features would be developed further in an enterprise level version of this application.

